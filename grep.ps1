﻿Param(
    [Parameter(Mandatory=$true,Position=0)]
    [String] $Pattern,
    [Parameter(Mandatory=$false)]
    [String] $Path,
    [Parameter(Mandatory=$false)]
    [String] $Filter,
    [Parameter(Mandatory=$false)]
    [switch] $ReturnFilenamesOnly,
    [Parameter(Mandatory=$false)]
    [switch] $MatchingLineCount
)

$files = $null

if ($Path) {
    if ($Filter) {
        $files = Get-ChildItem -Recurse -Path $Path -Filter $Filter | Where-Object {(Test-Path -Path $_.FullName -PathType Leaf) -eq $true}
    } else {
        $files = Get-ChildItem -Recurse -Path $Path | Where-Object {(Test-Path -Path $_.FullName -PathType Leaf) -eq $true}
    }
} else {
    if ($Filter) {
        $files = Get-ChildItem -Recurse -Path . -Filter $Filter | Where-Object {(Test-Path -Path $_.FullName -PathType Leaf) -eq $true}
    } else {
        $files = Get-ChildItem -Recurse -Path . | Where-Object {(Test-Path -Path $_.FullName -PathType Leaf) -eq $true}
    }
}

$results = $files | Select-String -Pattern ([regex]::escape($Pattern))

if ($ReturnFilenamesOnly.IsPresent) {
    $results | Select-Object -Unique Path
} elseif ($MatchingLineCount.IsPresent) {
    ($results | Select-Object Path).Length
} else {
    $results
}
﻿$installers = Get-ChildItem -Filter 7z*.msi
msiexec.exe /i $installers[0].FullName MSIINSTALLPERUSER=1

$userPath = (Get-ItemProperty -path HKCU:\Environment\ -Name Path).Path
if (-not ($userPath).Contains("7-Zip")) {
    if ($userPath.EndsWith(";")) {
        Set-ItemProperty -path HKCU:\Environment\ -Name Path -Value "$userPath$env:USERPROFILE\AppData\Local\Programs\7-Zip"
    } else {
        Set-ItemProperty -path HKCU:\Environment\ -Name Path -Value "$userPath;$env:USERPROFILE\AppData\Local\Programs\7-Zip"
    }
}
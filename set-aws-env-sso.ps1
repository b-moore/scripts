﻿rm ~\.aws\credentials -ErrorAction SilentlyContinue

aws sso login --profile DeveloperAccess-686490765147

$files = Get-ChildItem -Recurse -Path $env:USERPROFILE\.aws\sso\cache
$results = $files | Select-String -Pattern ([regex]::escape("accessToken"))
$results | Select-Object -Unique Path

$rawJson = Get-Content $results[0].Path -Raw
$json = ConvertFrom-Json $rawJson

$headers = @{"x-amz-sso_bearer_token"=($json.accessToken);}
$response = Invoke-RestMethod -Uri "https://portal.sso.us-east-1.amazonaws.com/federation/credentials?role_name=DeveloperAccess&account_id=686490765147" -Headers $headers

$accessKey = $response.roleCredentials.accessKeyId
$secretAccessKey = $response.roleCredentials.secretAccessKey
$sessionToken = $response.roleCredentials.sessionToken

$file = @"
[default]
aws_access_key_id=$accessKey
aws_secret_access_key=$secretAccessKey
aws_session_token=$sessionToken
"@

$file | Out-File ~\.aws\credentials -Encoding ascii
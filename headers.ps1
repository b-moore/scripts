﻿if (-not $args[0] -or (-not $args[1])) {
    Write-Host "Re-run with username and password as arguments for basic auth support"
    $headers = @{"Content-Type"="application/json"}
    return $headers
}
$base64AuthInfo = [Convert]::ToBase64String([Text.Encoding]::ASCII.GetBytes(("{0}:{1}" -f $args[0],$args[1])))
$headers = @{Authorization=("Basic {0}" -f $base64AuthInfo); "Content-Type"="application/json"}
return $headers

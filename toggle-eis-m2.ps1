if (Test-Path ~\.m2\settings.xml) {
	$null = New-Item -ItemType Directory -Path ~\.m2\temp -Force
	mv ~\.m2\settings.xml ~\.m2\temp\
	mv ~\.m2\settings-security.xml ~\.m2\temp\
	Write-Host "EIS settings deactivated"
} else {
	mv ~\.m2\temp\* ~\.m2\
	Write-Host "EIS settings active"
}
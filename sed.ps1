﻿$escapedSearchMap = @{
    'Data Source=ARGTPAWN-DB-DWH' = 'Data Source=ArgusSQLFci2017.aws.aflac.com'
    "Data Source=14tpawndb001" = "Data Source=ArgusSQLFci2017.aws.aflac.com"
    "Data Source=vm-win-p-dwh" = "Data Source=ArgusSQLFci2017.aws.aflac.com"
    '<SSIS:Property SSIS:Name="Value">14tpawndb001</SSIS:Property>' = '<SSIS:Property SSIS:Name="Value">ArgusSQLFci2017.aws.aflac.com</SSIS:Property>'
    "D:\Program Files\" = "\\14tpawnfs001\share\"
    "\\14tpawnfs001(?!\Shared)" = "\\14tpawnfs001\share"
    "\\argtpawn-db-dwh" = "\\14tpawnfs001\share"
    "\\14tpawnfs001\shared" = "\\14tpawnfs001\share"
    "\\14tpawnfs001\ftpdata$" = "\\14tpawnfs001\share\FTPData"
    "\\14tpawndb001\D$" = "\\14tpawnfs001\share"
    '\\14tpawnfs001\D$' = "\\14tpawnfs001\share"
    "D:\" = "\\14tpawnfs001\share\"
    "\\argtpawn-db-dwh\c$" = "\\14tpawnfs001\share"
    "I:\" = "\\14tpawnfs001\share\"
    'SmtpServer=webmail.ciocentral.us;UseWindowsAuthentication=False;EnableSsl=False' = 'SmtpServer=smtp.aflac.com;UseWindowsAuthentication=False;EnableSsl=True'
    'SmtpServer=argusdentalvision-com.mail.protection.outlook.com;UseWindowsAuthentication=False;EnableSsl=False' = 'SmtpServer=smtp.aflac.com;UseWindowsAuthentication=False;EnableSsl=True'
    '\\14tpawndb001\SSIS\' = '\\14tpawnfs001\share\ssis\'
    '14tpawnfs001\\Shared' = '14tpawnfs001\\share'
    '@enabled=1' = '@enabled=0'
    '14TPAWNDB003' = 'ArgusSQLFci2019.aws.aflac.com'
    'TCS_DatabaseMail' = 'DBMailDWH'
    'ARGUSTEAM\ssisuser' = 'AFLAWS\Argus-SSIS-user'
    'ARGUSTEAM\acuity3' = 'AFLAWS\Argus-SSIS-user'
}

$files = Get-ChildItem . -Recurse -Exclude .git,*.user,*.sln,*.suo

foreach ($file in $files) { 
    
    if (!(Test-Path -Path $file -PathType Leaf)) {
        continue
    }

    $fileName = $file.FullName
    $fileContent = Get-Content $file -Raw
    
    $hasher = [System.Security.Cryptography.HashAlgorithm]::Create("sha256")
    $hashBytes = $hasher.ComputeHash([System.Text.Encoding]::UTF8.GetBytes($fileContent))
    $hash = [System.BitConverter]::ToString($hashBytes)

    foreach ($pair in $escapedSearchMap.GetEnumerator()) {
        $fileContent = $fileContent -replace ([regex]::escape($pair.Name)), $pair.Value
    }

    $newHashBytes = $hasher.ComputeHash([System.Text.Encoding]::UTF8.GetBytes($fileContent))
    $newHash = [System.BitConverter]::ToString($newHashBytes)
    if ($hash -ne $newHash) {
        Write-Host "Changing $fileName"
        $fileContent | Out-File -FilePath $file.FullName -Encoding default
    }
}